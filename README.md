BookDiary

Application for writing down the books you have read, writing your own opinion about book and saving it.
BookDiary uses Firebase for logging in and saving books into the database.

Book

You can save basic information about the book, such as name, author, genre, when you started reading, stopped reading, rating and number of pages.
This information is immidiately stored in firebase database.

 
