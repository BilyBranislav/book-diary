package com.branislavbily.bookdiary.Preferences;

import android.content.Context;
import android.content.SharedPreferences;

public class PreferencesHelper {
    private static final String KEY_USER = "user_uid";
    private static final String KEY_PREFERENCES_FILE = "preferences";

    private static SharedPreferences getSharedPreferences(Context context) {
        return context.getSharedPreferences(KEY_PREFERENCES_FILE, 0);
    }

    public static void setUID(String uid, Context context) {
        getSharedPreferences(context)
                .edit()
                .putString(KEY_USER, uid)
                .apply();
    }

    public static String getUID(Context context) {
        return getSharedPreferences(context).getString(KEY_USER, "");
    }

    public static boolean isLoggedIn(Context context) {
        return !getSharedPreferences(context).getString(KEY_USER, "").isEmpty();
    }

    public static void logout(Context context) {
        getSharedPreferences(context)
                .edit()
                .putString(KEY_USER, "")
                .apply();
    }
}
