package com.branislavbily.bookdiary.Preferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;

import com.branislavbily.bookdiary.Modules.PreferencesModul;
import com.branislavbily.bookdiary.R;

public class SharedPref {

    SharedPreferences sharedPreferences;

    public SharedPref(Context context) {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public void changeToDarkModeState(boolean state) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(PreferencesModul.DARK_MODE, state);
        editor.commit();
    }

    public boolean getDarkModeState() {
        return sharedPreferences.getBoolean(PreferencesModul.DARK_MODE, false);
    }

    public void changeTheLastFragmentID(int id) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(PreferencesModul.FRAGMENT_ID, id);
        editor.commit();
    }

    public int getLastFragmentID() {
        return sharedPreferences.getInt(PreferencesModul.FRAGMENT_ID, 0);
    }
}
