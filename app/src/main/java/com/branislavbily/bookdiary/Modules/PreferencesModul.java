package com.branislavbily.bookdiary.Modules;

public interface PreferencesModul {

    String DARK_MODE = "darkmode";
    String REASON_ACTIVITY_WAS_CHANGED = "reasonForActivityChange";
    String FRAGMENT_ID = "fragmentID";
}
