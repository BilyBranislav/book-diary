package com.branislavbily.bookdiary.Modules;

public interface BookModul {

    String KEY_TITLE = "title";
    String KEY_RATING = "rating";
    String KEY_AUTHOR = "author";
    String KEY_GENRE = "genre";
    String KEY_STARTED_READING = "startedReading";
    String KEY_FINISHED_READING = "finishedReading";
    String KEY_NUMBER_OF_PAGES = "numberOfPages";
    String KEY_DAY_SAVED = "dateSaved";
    String KEY_DAY_UPDATED = "dateUpdated";
}
