package com.branislavbily.bookdiary.view.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.branislavbily.bookdiary.Modules.BookModul;
import com.branislavbily.bookdiary.R;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.Query;

public class BookNoteAdapter extends FirestoreAdapter<BookNoteAdapter.NoteHolder> {

    private OnBookClickListener onBookClickListener;

    public BookNoteAdapter(Query query, OnBookClickListener onBookClickListener) {
        super(query);
        this.onBookClickListener = onBookClickListener;

    }
    @NonNull
    @Override
    public NoteHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.note_item, viewGroup, false);
        return new NoteHolder(v, onBookClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull NoteHolder noteHolder, int i) {
        noteHolder.setData(getSnapshot(i));
    }

    class NoteHolder extends RecyclerView.ViewHolder {

        private View itemView;
        private OnBookClickListener onBookClickListener;

        private TextView textViewTitle;
        private TextView textViewAuthor;
        private TextView textViewGenre;

        NoteHolder(@NonNull View itemView, OnBookClickListener onBookClickListener) {
            super(itemView);
            this.itemView = itemView;
            this.onBookClickListener = onBookClickListener;
        }

        void setData(final DocumentSnapshot documentSnapshot) {
            textViewTitle = itemView.findViewById(R.id.textViewTitle);
            textViewAuthor = itemView.findViewById(R.id.textViewAuthor);
            textViewGenre = itemView.findViewById(R.id.textViewGenre);

            textViewTitle.setText(documentSnapshot.getString(BookModul.KEY_TITLE));
            Context context = itemView.getContext();
            String authorText = context.getResources().getString(R.string.author) + " " + documentSnapshot.getString(BookModul.KEY_AUTHOR);
            textViewAuthor.setText(authorText);
            String genreText  = context.getResources().getString(R.string.genre) + " "  + documentSnapshot.getString(BookModul.KEY_GENRE);
            textViewGenre.setText(genreText);

            itemView.setOnClickListener(v -> onBookClickListener.onBookClick(documentSnapshot.getId()));
        }

    }

    @Override
    public int getItemCount() {
        return super.getItemCount();
    }

    public interface OnBookClickListener {
        void onBookClick(String firebaseStoreID);
    }
}
