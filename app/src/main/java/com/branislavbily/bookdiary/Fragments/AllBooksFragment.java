package com.branislavbily.bookdiary.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.branislavbily.bookdiary.Activities.BookInformationActivity;
import com.branislavbily.bookdiary.Modules.BookModul;
import com.branislavbily.bookdiary.Preferences.PreferencesHelper;
import com.branislavbily.bookdiary.R;
import com.branislavbily.bookdiary.view.Adapter.BookNoteAdapter;
import com.google.firebase.FirebaseApp;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

public class AllBooksFragment extends Fragment implements BookNoteAdapter.OnBookClickListener {
    private FirebaseFirestore firestoreInstance;
    private BookNoteAdapter bookNoteAdapter;
    private RecyclerView recyclerView;
    private View rootView;

    private TextView textViewNoBookAdded;
    private TextView textViewStartByAddingNewBook;
    private ImageView imageViewBook;
    private int checkedItem;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.all_books_fragment, container, false);
        recyclerView = rootView.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        Query query = createQueryFromFirestore(BookModul.KEY_TITLE, true);
        bookNoteAdapter = new BookNoteAdapter(query, this);
        recyclerView.setAdapter(bookNoteAdapter);
        bookNoteAdapter.startListening();
        observeFirebaseData(query);
        return rootView;
    }

    private void updateRecyclerView(String orderBy, boolean isAscending) {
        Query query = createQueryFromFirestore(orderBy, isAscending);
        bookNoteAdapter = new BookNoteAdapter(query, this);
        recyclerView.setAdapter(bookNoteAdapter);
        bookNoteAdapter.startListening();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        FirebaseApp.initializeApp(getActivity().getApplicationContext());
        firestoreInstance = FirebaseFirestore.getInstance();
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        checkedItem = 0;
        //TODO Change this variable to SharedPreferences
    }

    private Query createQueryFromFirestore(String orderBy, boolean isAscending) {
        if (isAscending) {
            return firestoreInstance
                    .collection(PreferencesHelper.getUID(getActivity()))
                    .orderBy(orderBy, Query.Direction.ASCENDING);
        }
        return firestoreInstance
                .collection(PreferencesHelper.getUID(getActivity()))
                .orderBy(orderBy, Query.Direction.DESCENDING);
    }

    private void observeFirebaseData(Query query) {
        query.addSnapshotListener((queryDocumentSnapshots, e) -> {
            setUpConnections();
            if (queryDocumentSnapshots != null && !queryDocumentSnapshots.isEmpty()) {
                setViewsInvisible();
            } else {
                setViewsVisible();
            }
        });
    }

    private void setUpConnections() {
        textViewNoBookAdded = rootView.findViewById(R.id.textViewNoBooksAddedToLibrary);
        textViewStartByAddingNewBook = rootView.findViewById(R.id.textViewStartByAddingNewBook);
        imageViewBook = rootView.findViewById(R.id.imageViewBook);
    }

    private void setViewsVisible() {
        textViewNoBookAdded.setVisibility(View.VISIBLE);
        textViewStartByAddingNewBook.setVisibility(View.VISIBLE);
        imageViewBook.setVisibility(View.VISIBLE);
    }

    private void setViewsInvisible() {
        textViewNoBookAdded.setVisibility(View.INVISIBLE);
        textViewStartByAddingNewBook.setVisibility(View.INVISIBLE);
        imageViewBook.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onBookClick(String firebaseStoreID) {
        Intent intent = new Intent(getActivity(), BookInformationActivity.class);
        intent.putExtra("firebaseStoreID", firebaseStoreID);
//        Bundle bundle = ActivityOptions.makeCustomAnimation(getActivity(), R.anim.slide_in_right, R.anim.slide_out_left).toBundle();
        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    private void onSortByOptionItemSelected() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        String[] choices = createChoices();

        builder.setSingleChoiceItems(choices, checkedItem, (dialog, which) -> {
            switch (which) {
                case 0: {
                    updateRecyclerView(BookModul.KEY_TITLE, true);
                    checkedItem = 0;
                    dialog.cancel();
                    break;
                }
                case 1: {
                    updateRecyclerView(BookModul.KEY_GENRE, true);
                    checkedItem = 1;
                    dialog.cancel();
                    break;
                }
                case 2: {
                    updateRecyclerView(BookModul.KEY_AUTHOR, true);
                    checkedItem = 2;
                    dialog.cancel();
                    break;
                }
                case 3: {
                    updateRecyclerView(BookModul.KEY_DAY_SAVED, true);
                    checkedItem = 3;
                    dialog.cancel();
                    break;
                }
                case 4: {
                    updateRecyclerView(BookModul.KEY_DAY_SAVED, false);
                    checkedItem = 4;
                    dialog.cancel();
                    break;
                }
            }
        });

        builder.setTitle(getResources().getString(R.string.sortBy));
        builder.setNeutralButton("Cancel", (dialog, which) -> {
            dialog.cancel();
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private String[] createChoices() {
        return new String[]{
                "Title",
                "Genre",
                "Author",
                "Date created (oldest)",
                "Date created (most recent)"
        };
    }

    @Override
    public void onPause() {
        super.onPause();
        if (bookNoteAdapter != null) {
            bookNoteAdapter.stopListening();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        updateRecyclerView(BookModul.KEY_TITLE, true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main_activity_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.sortBy: {
                onSortByOptionItemSelected();
                return true;
            }
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
