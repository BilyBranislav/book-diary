package com.branislavbily.bookdiary;

public class Book {
    private String title;
    private String author;
    private String genre;
    private String startedReading;
    private String finishedReading;
    private String dateSaved;
    private String dateUpdated;
    private int numberOfPages;
    private int rating;
    private boolean favourite;

    public Book() {
        //Firestore needs an empty contructor
    }



    public Book(String title, String author, String genre, String startedReading, String finishedReading, String dateSaved, String dateUpdated, int numberOfPages, int rating, boolean favourite) {
        this.title = title;
        this.author = author;
        this.genre = genre;
        this.startedReading = startedReading;
        this.finishedReading = finishedReading;
        this.numberOfPages = numberOfPages;
        this.dateSaved = dateSaved;
        this.dateUpdated = dateUpdated;
        this.rating = rating;
        this.favourite = favourite;
    }



    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public String getGenre() {
        return genre;
    }

    public String getStartedReading() {
        return startedReading;
    }

    public String getFinishedReading() {
        return finishedReading;
    }

    public int getNumberOfPages() {
        return numberOfPages;
    }

    public int getRating() {
        return rating;
    }

    public boolean getFavourite() {
        return favourite;
    }

    public String getDateSaved() {
        return dateSaved;
    }

    public String getDateUpdated() {
        return dateUpdated;
    }

}
