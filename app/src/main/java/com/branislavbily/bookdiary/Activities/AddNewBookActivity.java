package com.branislavbily.bookdiary.Activities;

import android.content.Intent;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.Toast;

import com.branislavbily.bookdiary.Book;
import com.branislavbily.bookdiary.Preferences.PreferencesHelper;
import com.branislavbily.bookdiary.R;
import com.branislavbily.bookdiary.Preferences.SharedPref;
import com.google.firebase.FirebaseApp;
import com.google.firebase.firestore.FirebaseFirestore;

import java.text.DateFormat;
import java.util.Date;

public class AddNewBookActivity extends AppCompatActivity {

    private EditText editTextTitle, editTextAuthor, editTextGenre, editTextStartedReading, editTextFinishedReading, editTextPages;
    private NumberPicker numberPicker;
    private FirebaseFirestore firestoreInstance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FirebaseApp.initializeApp(this);
        firestoreInstance = FirebaseFirestore.getInstance();
        setThemeFromPreferences();
        setContentView(R.layout.activity_add_new_book);
        Toolbar toolbar = findViewById(R.id.toolbarAddNewBook);
        toolbar.inflateMenu(R.menu.new_note_menu);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_close);
        toolbar.setNavigationOnClickListener(v -> NavUtils.navigateUpTo(AddNewBookActivity.this,
                new Intent(AddNewBookActivity.this, MainActivity.class)));
        setTitle("Add note");

        connectEditTexts();
    }

    private void setThemeFromPreferences() {
        SharedPref sharedPreferences = new SharedPref(this);
        if (sharedPreferences.getDarkModeState()) {
            Log.i("darktheme", "on");
            setTheme(R.style.darktheme);
        } else {
            Log.i("darktheme", "off");
            setTheme(R.style.AppTheme);
        }
    }

    private void connectEditTexts() {
        editTextTitle = findViewById(R.id.editTextTitle);
        editTextAuthor = findViewById(R.id.editTextAuthor);
        editTextGenre = findViewById(R.id.editTextGenre);
        editTextStartedReading = findViewById(R.id.editTextStartedReading);
        editTextFinishedReading = findViewById(R.id.editTextFinishedReading);
        editTextPages = findViewById(R.id.editTextNumberOfPage);
        numberPicker = findViewById(R.id.numberPicker);

        numberPicker.setMinValue(0);
        numberPicker.setMaxValue(10);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.new_note_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.saveNote: saveBook();
            default: return super.onOptionsItemSelected(item);
        }
    }

    private void saveBook() {
        if (!inputCheckedSuccessfully()) {
            Toast.makeText(this, "Please make sure you are entering all values correctly", Toast.LENGTH_SHORT).show();
        } else {
            String title = formatUserInput(editTextTitle.getText().toString());
            String author = formatUserInput(editTextAuthor.getText().toString());
            String genre = formatUserInput(editTextGenre.getText().toString());
            String started = formatUserInput(editTextStartedReading.getText().toString());
            String finished = formatUserInput(editTextFinishedReading.getText().toString());
            int pages = Integer.parseInt(editTextPages.getText().toString());
            int rating = numberPicker.getValue();
            String currentDateTime = DateFormat.getDateTimeInstance().format(new Date());

            firestoreInstance.collection(PreferencesHelper.getUID(this)).add(new Book(title, author, genre, started, finished, currentDateTime, currentDateTime, pages, rating, false));

            finish();

        }
    }
    private boolean inputCheckedSuccessfully() {
        //Later change to required and optional information

        if (TextUtils.isEmpty(editTextTitle.getText())) {
            return false;
        }
        if (TextUtils.isEmpty(editTextAuthor.getText())) {
            return false;
        }
        if (TextUtils.isEmpty(editTextGenre.getText())) {
            return false;
        }
        if (TextUtils.isEmpty(editTextStartedReading.getText())) {
            return false;
        }
        if (TextUtils.isEmpty(editTextFinishedReading.getText())) {
            return false;
        }
        if (TextUtils.isEmpty(editTextPages.getText())) {
            return false;
        }
        return true;
    }

    private String formatUserInput(String input) {
        input = input.trim().toLowerCase();
        return input.substring(0, 1).toUpperCase() + input.substring(1);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setThemeFromPreferences();
    }

    @Override
    protected void onPause() {
        super.onPause();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
}
