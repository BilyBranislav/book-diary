package com.branislavbily.bookdiary.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Switch;

import com.branislavbily.bookdiary.Fragments.AllBooksFragment;
import com.branislavbily.bookdiary.Fragments.CathegoriesFragment;
import com.branislavbily.bookdiary.Fragments.ProfileFragment;
import com.branislavbily.bookdiary.Fragments.SearchFragment;
import com.branislavbily.bookdiary.Fragments.SecretBooksFragment;
import com.branislavbily.bookdiary.Fragments.SettingsFragment;
import com.branislavbily.bookdiary.Modules.PreferencesModul;
import com.branislavbily.bookdiary.Preferences.PreferencesHelper;
import com.branislavbily.bookdiary.Preferences.SharedPref;
import com.branislavbily.bookdiary.R;
import com.firebase.ui.auth.AuthUI;
import com.google.firebase.FirebaseApp;

import java.util.List;


public class MainActivity extends AppCompatActivity {

    private SharedPref sharedPreferences;


    private DrawerLayout drawer;
    private NavigationView navigationView;
    private int THEME_NOT_CHANGED = 0;
    private int THEME_WAS_CHANGED = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        FirebaseApp.initializeApp(this);

        if (!PreferencesHelper.isLoggedIn(this)) {
            startActivity(new Intent(this, LoginActivity.class));
        }
        sharedPreferences = new SharedPref(this);
        setThemeFromPreferences();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUpNavigationDrawer();
        setUpSwitch();

        int reason = getIntent().getIntExtra(PreferencesModul.REASON_ACTIVITY_WAS_CHANGED, THEME_NOT_CHANGED);
        if (reason == THEME_WAS_CHANGED) {
            drawer.openDrawer(GravityCompat.START);
        }
        //This will load the fragment when user rotated his device, changed the text size etc.
        if (savedInstanceState == null) {
            navigationView.setCheckedItem(R.id.nav_all_books);
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new AllBooksFragment()).commit();
        }



    }

    private void setThemeFromPreferences() {
        if (sharedPreferences.getDarkModeState()) {
            setTheme(R.style.darktheme);
        } else {
            setTheme(R.style.AppTheme);
        }
    }

    private void setUpNavigationDrawer() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        drawer = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(menuItem -> {
            switch (menuItem.getItemId()) {
                case R.id.nav_all_books: {
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new AllBooksFragment()).commit();
                    break;
                }
                case R.id.nav_search: {
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new SearchFragment()).commit();
                    break;
                }
                case R.id.nav_cathegories: {
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new CathegoriesFragment()).commit();
                    break;
                }
                case R.id.nav_secret_books: {
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new SecretBooksFragment()).commit();
                    break;
                }
                case R.id.nav_profile: {
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new ProfileFragment()).commit();
                    break;
                }
                case R.id.nav_settings: {
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new SettingsFragment()).commit();
                    break;
                }
                case R.id.nav_logout: {
                    logout();
                }
            }
            drawer.closeDrawer(GravityCompat.START);
            return true;
        });
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
    }

    private void setUpSwitch() {
        navigationView.getMenu().findItem(R.id.nav_dark_mode_switch).setActionView(new Switch(this));
        navigationView.getMenu().findItem(R.id.nav_dark_mode_switch).setCheckable(true);
        if(sharedPreferences.getDarkModeState()) {
            ((Switch) navigationView.getMenu().findItem(R.id.nav_dark_mode_switch).getActionView()).setChecked(true);
        }
        ((Switch) navigationView.getMenu().findItem(R.id.nav_dark_mode_switch).getActionView())
                .setOnCheckedChangeListener((buttonView, isChecked) -> {
                    if (isChecked) {
                        changeToDarkMode();
                    } else {
                        changeToLightMode();
                    }
                });
    }

    private void changeToDarkMode() {
        sharedPreferences.changeToDarkModeState(true);
        restartAcitivity();
    }

    private void changeToLightMode() {
        sharedPreferences.changeToDarkModeState(false);
        restartAcitivity();
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    /**
     * This method stats and FirebaseUI kit, starts an SignOut sequence and listens on Complete
     * When is Task Completed, User is deleted from PreferencesHelper
     */

    private void logout() {
        AuthUI.getInstance()
                .signOut(this)
                .addOnCompleteListener(task -> {
                    // Delete user and finish activity
                    PreferencesHelper.logout(MainActivity.this);
                    startActivity(new Intent(this, LoginActivity.class));
                });
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!PreferencesHelper.isLoggedIn(this)) {
            startActivity(new Intent(this, LoginActivity.class));
        } else {
            if (sharedPreferences.getDarkModeState()) {
                setTheme(R.style.darktheme);
                ((Switch) navigationView.getMenu().findItem(R.id.nav_dark_mode_switch).getActionView()).setChecked(true);

            } else {
                setTheme(R.style.AppTheme);
            }
            loadLastFragment();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        saveLastFragment();
    }

    private int getVisibleFragment() {
        FragmentManager fragmentManager = MainActivity.this.getSupportFragmentManager();
        List<Fragment> fragments = fragmentManager.getFragments();
        for(Fragment fragment : fragments){
            if(fragment != null && fragment.isVisible())
                return fragment.getId();
        }
        return 0;
    }

    private Fragment getFragmentFromID(int id) {
        FragmentManager fragmentManager = MainActivity.this.getSupportFragmentManager();
        List<Fragment> fragments = fragmentManager.getFragments();
        for(Fragment fragment : fragments){
            if(fragment.getId() == id)
                return fragment;
        }
        return null;
    }

    public void onFloatingButtonPressed(View view) {
        startActivity(new Intent(MainActivity.this, AddNewBookActivity.class));
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    private void restartAcitivity() {
        Intent intent = getIntent();
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        intent.putExtra(PreferencesModul.REASON_ACTIVITY_WAS_CHANGED, THEME_WAS_CHANGED);
        finish();
        overridePendingTransition(R.anim.no_animation, R.anim.no_animation);
        startActivity(intent);
    }

    /**
     *
     */
    private void saveLastFragment() {
        int id = getVisibleFragment();
        if (id != 0) {
            sharedPreferences.changeTheLastFragmentID(id);
        }
    }
    //Loads last fragment that user was in when he exited the app, loads all books fragment by default
    private void loadLastFragment() {
        Fragment lastFragment = getFragmentFromID(sharedPreferences.getLastFragmentID());
        if(lastFragment != null) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, lastFragment).commit();
        } else {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new AllBooksFragment()).commit();
        }

    }

}
