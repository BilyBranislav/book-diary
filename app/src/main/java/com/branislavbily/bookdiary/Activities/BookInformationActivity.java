package com.branislavbily.bookdiary.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.branislavbily.bookdiary.Book;
import com.branislavbily.bookdiary.DeleteBookHandler;
import com.branislavbily.bookdiary.Modules.BookModul;
import com.branislavbily.bookdiary.Preferences.PreferencesHelper;
import com.branislavbily.bookdiary.R;
import com.branislavbily.bookdiary.Preferences.SharedPref;
import com.google.firebase.FirebaseApp;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FieldPath;
import com.google.firebase.firestore.FirebaseFirestore;

import java.text.DateFormat;
import java.util.Date;

public class BookInformationActivity extends AppCompatActivity {

    private FirebaseFirestore firestoreInstance;
    private String firebaseStoreID;
    private String dateCreated;

    private Menu activityMenu;

    private ImageView imageViewFavourite;
    private EditText editTextTitle;
    private EditText editTextAuthor;
    private EditText editTextGenre;
    private EditText editTextStartedReading;
    private EditText editTextFinishedReading;
    private EditText editTextNumberOfPages;
    private EditText editTextRating;

    private Toolbar toolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FirebaseApp.initializeApp(this);
        firestoreInstance = FirebaseFirestore.getInstance();
        setThemeFromPreferences();
        setContentView(R.layout.activity_book_information);

        connectViews();
        toolbar.inflateMenu(R.menu.new_note_menu);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow);
        toolbar.setNavigationOnClickListener(v -> NavUtils.navigateUpTo(BookInformationActivity.this,
                new Intent(BookInformationActivity.this, MainActivity.class)));
        disableEditTexts();
        getDocument();

    }

    private void setThemeFromPreferences() {
        SharedPref sharedPreferences = new SharedPref(this);
        if (sharedPreferences.getDarkModeState()) {
            setTheme(R.style.darktheme);
        } else {
            setTheme(R.style.AppTheme);
        }
    }

    private void connectViews() {
        imageViewFavourite = findViewById(R.id.imageViewFavourite);
        editTextTitle = findViewById(R.id.editTextInformationTitle);
        editTextAuthor = findViewById(R.id.editTextInformationAuthor);
        editTextGenre = findViewById(R.id.editTextInformationGenre);
        editTextStartedReading = findViewById(R.id.editTextInformationStartedReading);
        editTextFinishedReading = findViewById(R.id.editTextInformationFinishedReading);
        editTextNumberOfPages = findViewById(R.id.editTextInformationNumberOfPages);
        editTextRating = findViewById(R.id.editTextInformationRating);
        toolbar = findViewById(R.id.toolbarBookInformation);
    }

    private void disableEditTexts() {
        editTextTitle.setFocusable(false);
        editTextTitle.setCursorVisible(false);
        editTextAuthor.setFocusable(false);
        editTextAuthor.setCursorVisible(false);
        editTextGenre.setFocusable(false);
        editTextGenre.setCursorVisible(false);
        editTextStartedReading.setFocusable(false);
        editTextStartedReading.setCursorVisible(false);
        editTextFinishedReading.setFocusable(false);
        editTextFinishedReading.setCursorVisible(false);
        editTextNumberOfPages.setFocusable(false);
        editTextNumberOfPages.setCursorVisible(false);
        editTextRating.setFocusable(false);
        editTextRating.setCursorVisible(false);
    }

    private void getDocument() {
        firebaseStoreID = getIntent().getStringExtra("firebaseStoreID");
        firestoreInstance.collection(PreferencesHelper.getUID(this))
                .whereEqualTo(FieldPath.documentId(), firebaseStoreID)
                .limit(1)
                .addSnapshotListener(((queryDocumentSnapshots, e) -> {
                    for (DocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                        if(e != null) {
                            return;
                        }
                        if(documentSnapshot != null) {
                            setTextToViews(documentSnapshot);
                        }
                    }
                }));
    }

    private void setTextToViews(DocumentSnapshot documentSnapshot) {
        setTitle(documentSnapshot.getString(BookModul.KEY_TITLE));

        editTextTitle.setText(documentSnapshot.getString(BookModul.KEY_TITLE));
        editTextAuthor.setText(documentSnapshot.getString(BookModul.KEY_AUTHOR));
        editTextGenre.setText(documentSnapshot.getString(BookModul.KEY_GENRE));
        editTextStartedReading.setText(documentSnapshot.getString(BookModul.KEY_STARTED_READING));
        editTextFinishedReading.setText(documentSnapshot.getString(BookModul.KEY_FINISHED_READING));
        editTextNumberOfPages.setText(Long.toString(documentSnapshot.getLong(BookModul.KEY_NUMBER_OF_PAGES)));
        editTextRating.setText(Long.toString(documentSnapshot.getLong(BookModul.KEY_RATING)));
        dateCreated = documentSnapshot.getString(BookModul.KEY_DAY_SAVED);

        if(documentSnapshot.getBoolean("favourite") != null && documentSnapshot.getBoolean("favourite")) {
            imageViewFavourite.setImageResource(R.drawable.ic_favorite_full_24dp);
        } else {
            imageViewFavourite.setImageResource(R.drawable.ic_favorite_border_black_24dp);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        activityMenu = menu;
        int mainMenu = R.menu.book_information_menu;
        getMenuInflater().inflate(mainMenu, activityMenu);
        return true;
    }

    public void onCreateOptionsMenu(Menu menu, int menuID) {
        activityMenu = menu;
        activityMenu.clear();
        getMenuInflater().inflate(menuID, activityMenu);
    }

    private void changeMenus(int menuID) {
        if(activityMenu == null) return;
        onCreateOptionsMenu(activityMenu, menuID);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setThemeFromPreferences();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.deleteBook: onDeleteMenuOptionSelected();return true;
            case R.id.updateBook: onUpdateBookMenuOptionSelected();return true;
            case R.id.favouriteMenu: onFavouriteMenuOptionSelected(); return true;
            case R.id.saveNote: onSaveMenuOptionSelected();return true;
            default: return super.onOptionsItemSelected(item);
        }
    }

    private void onDeleteMenuOptionSelected() {
        deleteDialog();
    }

    private void deleteDialog() {
        DeleteBookHandler deleteBookHandler = new DeleteBookHandler(this, firestoreInstance, firebaseStoreID);
        deleteBookHandler.createDeleteDialog();
    }

    private void onUpdateBookMenuOptionSelected() {
        changeMenus(R.menu.new_note_menu);
        enableEditTexts();
    }

    private void enableEditTexts() {
        editTextTitle.setFocusableInTouchMode(true);
        editTextTitle.setCursorVisible(true);
        editTextAuthor.setFocusableInTouchMode(true);
        editTextAuthor.setCursorVisible(true);
        editTextGenre.setFocusableInTouchMode(true);
        editTextGenre.setCursorVisible(true);
        editTextStartedReading.setFocusableInTouchMode(true);
        editTextStartedReading.setCursorVisible(true);
        editTextFinishedReading.setFocusableInTouchMode(true);
        editTextFinishedReading.setCursorVisible(true);
        editTextNumberOfPages.setFocusableInTouchMode(true);
        editTextNumberOfPages.setCursorVisible(true);
        editTextRating.setFocusableInTouchMode(true);
        editTextRating.setCursorVisible(true);
    }

    private void onFavouriteMenuOptionSelected() {

    }

    private void onSaveMenuOptionSelected() {
        disableEditTexts();
        Book book = getUpdatedBookData();
        Toast.makeText(this, "Changes saved successfully!", Toast.LENGTH_SHORT).show();
        firestoreInstance.collection(PreferencesHelper.getUID(this)).document(firebaseStoreID).set(book);
        changeMenus(R.menu.book_information_menu);
    }

    private Book getUpdatedBookData() {
        String title = editTextTitle.getText().toString();
        String author = editTextAuthor.getText().toString();
        String genre = editTextGenre.getText().toString();
        String startedReading = editTextStartedReading.getText().toString();
        String finishedReading = editTextFinishedReading.getText().toString();
        String currentDateTime = DateFormat.getDateTimeInstance().format(new Date());
        int numberOfPages = (int) Long.parseLong(editTextNumberOfPages.getText().toString());
        int rating = (int) Long.parseLong(editTextRating.getText().toString());

        return new Book(title, author, genre, startedReading, finishedReading, dateCreated, currentDateTime, numberOfPages, rating, false);
    }

    @Override
    protected void onPause() {
        super.onPause();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
}
