package com.branislavbily.bookdiary;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Gravity;
import android.widget.TextView;
import android.widget.Toast;

import com.branislavbily.bookdiary.Preferences.PreferencesHelper;
import com.google.firebase.firestore.FirebaseFirestore;

public class DeleteDialog extends AlertDialog {

    private Context context;
    private FirebaseFirestore firestoreInstance;
    private String firebaseStoreID;

    public DeleteDialog(@NonNull Context context, FirebaseFirestore firestoreInstance, String fireStoreID) {
        super(context);
        this.context = context;
        this.firebaseStoreID = fireStoreID;
        this.firestoreInstance = firestoreInstance;

        if(context instanceof Activity) {
            setOwnerActivity((Activity) context);
        }

        createDeleteDialog();
    }

    public void createDeleteDialog() {
        final AlertDialog alertDialog = new AlertDialog.Builder(context).create();

        TextView title = createTitle();
        alertDialog.setCustomTitle(title);

        TextView msg = createMessage();
        alertDialog.setView(msg);

        setNeutralButton(alertDialog);
        setNegativeButton(alertDialog);

        new Dialog(context);
        alertDialog.show();
    }

    private void setNeutralButton(final AlertDialog alertDialog) {
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL,"NO", (dialog, which) -> alertDialog.cancel());
    }

    private void setNegativeButton(AlertDialog alertDialog) {
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE,"YES", (dialog, which) -> {
            deleteBookFromDatabase();
            getOwnerActivity().finish();
        });
    }

    private TextView createTitle() {
        TextView title = new TextView(context);
        title.setText(R.string.are_you_sure_you_want_to_delete_this_book);
        title.setPadding(10, 10, 10, 10);
        title.setGravity(Gravity.CENTER);
        title.setTextColor(Color.BLACK);
        title.setTextSize(25);
        return title;
    }

    private TextView createMessage() {
        TextView msg = new TextView(context);
        msg.setText(R.string.you_cant_undo_this_change);
        msg.setGravity(Gravity.CENTER_HORIZONTAL);
        msg.setTextColor(Color.BLACK);
        msg.setTextSize(20);
        return msg;
    }

    private void deleteBookFromDatabase() {
        firestoreInstance.collection(PreferencesHelper.getUID(context)).document(firebaseStoreID)
                .delete().addOnSuccessListener(aVoid -> getOwnerActivity().finish()).addOnFailureListener(e -> {
            Toast.makeText(context, "Something went wrong", Toast.LENGTH_SHORT).show();
            Log.e("Writing to database", "Something went wrong");
        });
    }


}
