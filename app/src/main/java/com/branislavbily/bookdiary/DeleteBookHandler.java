package com.branislavbily.bookdiary;

import android.content.Context;

import com.google.firebase.firestore.FirebaseFirestore;

public class DeleteBookHandler {

    private Context context;
    private FirebaseFirestore firestoreInstance;
    private String firebaseStoreID;

    public DeleteBookHandler(Context context, FirebaseFirestore firestoreInstance, String firebaseStoreID) {
        this.firestoreInstance = firestoreInstance;
        this.firebaseStoreID = firebaseStoreID;
        this.context = context;
    }

    public void createDeleteDialog() {
        new DeleteDialog(context, firestoreInstance, firebaseStoreID);
    }
}
